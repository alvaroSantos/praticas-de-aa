FOLDS = 5
FEATURES = 16

import numpy as np
import matplotlib.pyplot as plt
import expand as exp
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import train_test_split #Novo
from sklearn.linear_model import LogisticRegression

def irange(start, stop):
    return range(start, stop + 1)

def standardise(data):
    '''data is a numpy array'''
    data_mean = np.mean(data, 0)
    data_std = np.std(data, 0)
    return (data-data_mean)/data_std

data = np.loadtxt("data.txt", delimiter=",")

np.random.shuffle(data)

#Expand expects to receive only the data's features
#So we separate them from the classifications
classifications = data[:, 0]
data = data[:, 1:]

data = standardise(data)

data = exp.poly_16features(data)

#Novo
training_X, test_X, training_Y, test_Y = train_test_split(data, classifications, test_size=0.33, stratify = classifications)

#Use 5-fold cross-validation
k_folds = StratifiedKFold(training_Y, FOLDS)

errors = []
best_features_amount = -1
best_error = -1

for feature in irange(2, FEATURES):
    training_error = 0
    validation_error = 0

    for remaining, removed in k_folds:
        reg = LogisticRegression(C=1e12, tol=1e-10)

        reg.fit(training_X[remaining, :feature], training_Y[remaining])
        probabilities = reg.predict_proba(training_X[:, :feature])[:, 1]

        training_error += np.mean((probabilities[remaining] - training_Y[remaining])**2)
        validation_error += np.mean((probabilities[removed] - training_Y[removed])**2)

    #Era disto...
    training_error /= FOLDS
    validation_error /= FOLDS

    if best_features_amount == -1 or best_error > validation_error + 0.001:
        best_error = validation_error
        best_features_amount = feature

    errors.append([training_error, validation_error])

errors = np.array(errors)

plt.figure()
plt.title("Logistic Regression w/ " + str(FOLDS) +  "-fold cross-validation")
plt.plot(np.array(irange(2, FEATURES)), errors[:, 0], "b-", label="Training error")
plt.plot(np.array(irange(2, FEATURES)), errors[:, 1], "r-", label="Validation error")
plt.legend()
plt.savefig("exercise1.png")
plt.show()
plt.close()

print("Best number of features: " + str(best_features_amount))
print("Validation error: " + str(best_error))

#Unconfirmed, but should be right
reg = LogisticRegression(C=1e12, tol=1e-10)
reg.fit(training_X[:, :best_features_amount], training_Y)
prob = reg.predict_proba(test_X[: , :best_features_amount])[:, 1]
test_error = np.mean((prob - test_Y)**2)
print("Test error: " + str(test_error))