FOLDS = 5
FEATURES = 16

import numpy as np
import matplotlib.pyplot as plt
import expand as exp
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression

def irange(start, stop):
    return range(start, stop + 1)

def standardise(data):
    '''data is a numpy array'''
    data_mean = np.mean(data, 0)
    data_std = np.std(data, 0)
    return (data-data_mean)/data_std

data = np.loadtxt("data.txt", delimiter=",")

np.random.shuffle(data)

#Expand expects to receive only the data's features
#So we separate them from the classifications
classifications = data[:, 0]
data = data[:, 1:]

#Is there really no problem with doing these 2 in this order though?
#Apparently there is!!!! Standardising after expanding the features gives strange results (= different from the PDF)
data = standardise(data)
data = exp.poly_16features(data)
#data = standardise(data)

'''
#Rejoin the data and shuffle it
data = np.column_stack((data, classifications))

#The index at which we'll split our data into the training and test sets
split_at = int(data.shape[0]/3)

test_set = data[:split_at]
training_set = data[split_at:]
'''

training_X, test_X, training_Y, test_Y = train_test_split(data, classifications, test_size=0.33, stratify = classifications)

#Use 5-fold cross-validation
k_folds = StratifiedKFold(training_Y, FOLDS)

errors = []
best_c = -1
best_error = -1

C = 1.0
for i in range(20):
    training_error = 0
    validation_error = 0

    for remaining, removed in k_folds:
        reg = LogisticRegression(penalty="l2", C=C, tol=1e-10)

        reg.fit(training_X[remaining, :FEATURES], training_Y[remaining])
        probabilities = reg.predict_proba(training_X[:, :FEATURES])[:, 1]

        training_error += np.mean((probabilities[remaining] - training_Y[remaining])**2)
        validation_error += np.mean((probabilities[removed] - training_Y[removed])**2)

    training_error /= FOLDS
    validation_error /= FOLDS

    if best_c == -1 or best_error > validation_error:
        best_error = validation_error
        best_c = C

    errors.append([C, training_error, validation_error])

    C *= 2

errors = np.array(errors)

plt.figure()
plt.title("Logistic Regression w/ " + str(FEATURES) + " features, varying regularisation patterns and " + str(FOLDS) +  "-fold cross-validation")
plt.plot(np.log(errors[:, 0]), errors[:, 1], "b-", label="Training error")
plt.plot(np.log(errors[:, 0]), errors[:, 2], "r-", label="Validation error")
plt.legend()
plt.savefig("exercise2.png")
plt.show()
plt.close()

print("Best regularisation parameter: " + str(best_c))
print("Validation error: " + str(best_error))

#Unconfirmed, but should be right
reg = LogisticRegression(C=1e12, tol=1e-10)
reg.fit(training_X[:, :FEATURES], training_Y)
prob = reg.predict_proba(test_X[: , :FEATURES])[:, 1]
test_error = np.mean((prob - test_Y)**2)
print("Test error: " + str(test_error))

#This is an extra (lifted from exercise 4)
def poly_mat(reg,X_data,feats,ax_lims):
    """create score matrix for contour
    """
    Z = np.zeros((200,200))
    xs = np.linspace(ax_lims[0],ax_lims[1],200)
    ys = np.linspace(ax_lims[2],ax_lims[3],200)
    X,Y = np.meshgrid(xs,ys)
    points = np.zeros((200,2))
    points[:,0] = xs
    for ix in range(len(ys)):
        points[:,1] = ys[ix]
        x_points=exp.poly_16features(points)[:,:feats]
        Z[ix,:] = reg.decision_function(x_points)
    return (X,Y,Z)

ax_lims=(-2.5,2.5,-2.5,2.5)
plt.figure(figsize=(8,8), frameon=False)
plt.axis(ax_lims)
reg = LogisticRegression(C=best_c, tol=1e-10)

reg.fit(training_X,training_Y)
plotX,plotY,Z = poly_mat(reg,training_X,16,ax_lims)
plt.contourf(plotX,plotY,Z,[-1e16,0,1e16], colors = ('b', 'r'),alpha=0.5)
plt.contour(plotX,plotY,Z,[0], colors = ('k'))
plt.plot(training_X[training_Y>0,0],training_X[training_Y>0,1],'or')
plt.plot(training_X[training_Y<=0,0],training_X[training_Y<=0,1],'ob')
plt.plot(test_X[test_Y>0,0],test_X[test_Y>0,1],'xr',mew=2)
plt.plot(test_X[test_Y<=0,0],test_X[test_Y<=0,1],'xb',mew=2)
plt.savefig('exercise4.png', dpi=300)
plt.show()
plt.close()