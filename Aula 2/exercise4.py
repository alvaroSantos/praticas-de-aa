#NEVER SHOWS LUDWIG'S LEFT IMAGE (PDF) WITH THE HUGE RANGES - WHEN SHOULD THAT HAPPEN THOUGH?

import numpy as np
import matplotlib.pyplot as plt
import math as m
from sklearn.linear_model import Ridge

def irange(start, stop):
    return range(start, stop + 1)

def expand(data, degree):
    expanded = np.zeros((np.shape(data)[0], degree + 1))    #Create a matrix for as many points as before, but where each point is of the form (x^1, x^2, ..., x^degree, y)
    expanded[:, -1] = data[:, -1]   #Keep all the Ys unaltered
    expanded[:, 0] = data[:, 0] #Keep all the Xs unaltered

    for i in range(2, degree + 1):  #Calcula as potências dos Xs originais e guarda-as
        expanded[:, i - 1] = data[:, 0] ** i

    return expanded

def load_data_ignore_first_row(file_name):
    '''Returns an array of ages and lengths in years and milimeters, respectively'''
    rows = []
    lines = open(file_name).readlines()
    for line in lines[1:]:
        parts = line[0:-1].split('\t')
        rows.append((int(parts[0]), int(parts[1])))
    return np.array(rows)

data = load_data_ignore_first_row("bluegills.txt")

x, y = (data[:, 0], data[:, 1])

def standardise(d):
    d_mean = np.mean(d, 0)
    d_std = np.std(d, 0)
    return (d-d_mean)/d_std

#We're still supposed to standardise the data, right?
x = standardise(x)
y = standardise(y)

data = expand(np.column_stack((x, y)), 5)   #Expand to degree 5 (= 5 features?)

np.random.shuffle(data)

def half_data(d):
    l = len(d)
    return d[0:m.ceil(l/2)], d[m.ceil(l/2):]

training_set, rest = half_data(data)
validation_set, testing_set = half_data(rest)

plot_data = []

best_lambda = -1
best_solver = None

best_validation_error = -1
lambdas = np.linspace(0.001, 10)
for l in lambdas:
    solver = Ridge(alpha = l, solver="cholesky", tol=0.00001)   #Calcula a regressão de Ridge com o lambda l (usando o algoritmo indicado no solver e dado uma pequena tolerância para que o algoritmo não demora 50000 anos)
    solver.fit(training_set[:, :-1], training_set[:, -1])   #Usando os Xs (1º param) e os Ys (2º param). Does this by finding the best thetas (the ones that minimize the J(thetas) expression [mean quadratic error + the extra term to penalize high thetas])
    predicted_Ys = solver.predict(validation_set[:, :-1])    #Prevê os valores de Y para os X de validação com base na hipótese encontrada ao fazer fit
    validation_error = np.mean((validation_set[:, -1] - predicted_Ys) ** 2)

    plot_data.append([l, validation_error])
    if best_lambda == -1 or validation_error < best_validation_error:
        best_lambda = l
        best_solver = solver
        best_validation_error = validation_error

plt.figure()

def plot_points(points, colour):
    plt.plot(points[:, 0], points[:, 1], colour + "o", markersize=2)

plot_points(np.array(plot_data), "b")

plt.title("Error vs Lambda")
plt.savefig("exercise4.png", dpi=600, figsize=(12, 8))
plt.show()
plt.close()

#This is an extra#
#The question is: is it properly implemented?
best_solver = Ridge(alpha = best_lambda, solver="cholesky", tol=0.00001)
best_solver.fit(training_set[:, :-1], training_set[:, -1])
predicted_Ys = solver.predict(testing_set[:, :-1])

plt.figure()

#coeffs are not from largest to lowest, in order
coeffs = np.append(best_solver.coef_[::-1], best_solver.intercept_)
Xs = np.linspace(min(data[:, 0]), max(data[:, 0]))
Ys = np.polyval(coeffs, Xs)

#This is an extra to the extra (plot another solution, not necessarily the worst despite the variable's name being worst_solver)
worst_solver = Ridge(alpha = (10.001 - best_lambda), solver="cholesky", tol=0.00001)
worst_solver.fit(training_set[:, :-1], training_set[:, -1])
wp_Ys = worst_solver.predict(testing_set[:, :-1])

w_coeffs = np.append(worst_solver.coef_[::-1], worst_solver.intercept_)
wXs = np.linspace(min(data[:, 0]), max(data[:, 0]))
wYs = np.polyval(w_coeffs, wXs)

plt.plot(wXs, wYs, linewidth=0.5)
#End of the extra's extra

plt.plot(Xs, Ys, linewidth=0.5)

def plot_points(points, colour):
    plt.plot(points[:, 0], points[:, -1], colour + "o", markersize=2)

plot_points(training_set, "b")
plot_points(validation_set, "g")
plot_points(testing_set, "r")

plt.title("Degree 5 polynomial found w/ Ridge regression @ lambda " + "{:.3f}".format(best_lambda))
plt.savefig("exercise4_extra.png", dpi=600, figsize=(12, 8))
plt.show()
plt.close()
#End of the extra