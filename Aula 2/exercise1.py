import numpy as np
import matplotlib.pyplot as plt
import math as m

#utility function for inclusive ranges
def irange(start, stop):
    return range(start, stop + 1)

def load_data_ignore_first_row(file_name):
    '''Returns an array of ages and lengths in years and milimeters, respectively'''
    rows = []
    lines = open(file_name).readlines()
    for line in lines[1:]:
        parts = line[0:-1].split('\t')
        rows.append((int(parts[0]), int(parts[1])))
    return np.array(rows)

#The pdf says we can use the loadtxt function, but the first line is trash...
data = load_data_ignore_first_row("bluegills.txt")

x, y = (data[:, 0], data[:, 1])

def standardise(d):
    d_mean = np.mean(d, 0)
    d_std = np.std(d, 0)
    return (d-d_mean)/d_std

#The pdf says to standardise. But why standardise instead of normalising?
#Why do we standardise the Ys? (Based on Lecture 4, we should only do it for Xs)
x = standardise(x)
y = standardise(y)

#Joins x and y back together into a 78x2 matrix)
data = np.column_stack((x, y))

#Shuffles the data, but keeps pairs (x, y) unbroken
np.random.shuffle(data)

#QUESTION: How do we split data into a non-divisor amount of groups?
#Right now we just add whatever to the first half, but is that correct?
def half_data(d):
    l = len(d)
    return d[0:m.ceil(l/2)], d[m.ceil(l/2):]

training_set, rest = half_data(data)
validation_set, testing_set = half_data(rest)

#--- The data has been standardised, shuffled and split into the 3 sets we need

#Stores the coefficients, errors and degrees for each best hypothesis of each model
#(polynomials from degrees 1 to 5, in this order)
best_hypotheses = []
best_validation_error = -1
best_coeffs = -1
best_degree = -1

#Calculates the validation errors
for i in irange(1, 5):
    coeffs = np.polyfit(training_set[:, 0], training_set[:, 1], i)
    predictions = np.polyval(coeffs, validation_set[:, 0])
    differences = validation_set[:, 1] - predictions
    mean_square_error = np.mean(differences**2)

    #Doesn't stricly need to be here (see comment below)
    #Condensed the calculation into 1 line, but it's the same as the above, except for another set of data
    training_error = np.mean((training_set[:, 1] - (np.polyval(coeffs, training_set[:, 0])))**2)

    best_hypotheses.append([coeffs, mean_square_error, i, training_error])
    if best_degree == -1 or mean_square_error < best_validation_error:
        best_validation_error = mean_square_error
        best_coeffs = coeffs
        best_degree = i

#^ Calculates the training error of every hypothesis
#Although we only really care about the best hypothesis' (= hypothesis with the least validation error)
#But we want to put the other ones in the graph's legends (to emulate Ludwig's graph)

plt.figure()

#Plot the best hypotheses
#É normal aqui as linhas darem diferentes das do Ludwig, certo?
#(Por seleccionarmos os conjuntos de treino, validação e teste aleatoriamente)
#Ou sou eu que estou a fazer algumas das contas mal?
for h in best_hypotheses:
    coeffs = h[0]
    validation_error = h[1]
    degree = h[2]
    training_error = h[3]

    parts = np.linspace(min(x), max(x), 100)
    images = np.polyval(coeffs, parts)
    plt.plot(parts, images, linewidth=0.5, label="{}/{:.4f}/{:.4f}".format(degree, training_error, validation_error))

def plot_points(points, colour):
    plt.plot(points[:, 0], points[:, 1], colour + "o", markersize=2)

#Plot the points
plot_points(training_set, "b")
plot_points(validation_set, "g")
plot_points(testing_set, "r")

plt.title("Bluegill sizes")
plt.legend()
plt.axis([-3, 3, -4, 2])    #Same as Ludwig's
plt.savefig("exercise1.png", dpi=600, figsize=(12, 8))
plt.show()
plt.close()

best_test_error = np.mean((testing_set[:, 1] - (np.polyval(best_coeffs, testing_set[:, 0])))**2)
print("Best hypothesis' test error: ", best_test_error, " || Degree: ", best_degree)