import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("polydata.csv", delimiter=";")

#debug
#print(data)

x, y = (data[:, 0], data[:, 1])

def points_to_plot(data, degree):
    coefs = np.polyfit(x, y, degree);

    partitions = np.linspace(0, max(x), 100)
    return (partitions, np.polyval(coefs, partitions))

deg3_points = points_to_plot(data, 3)
deg15_points = points_to_plot(data, 15)

point_string = 'ro' #red points
line_string = 'b-' #blue continuous lines

def draw_graph(points, name):
    plt.figure()
    plt.plot(points[0], points[1], line_string)
    plt.plot(x, y, point_string, markersize=3)  #markersize -> points' size
    plt.title("Degree: " + name[3:])
    plt.axis([0, 6, -1.5, 1.5])
    plt.savefig("exercise2_" + name +".png")
    plt.show()
    plt.close()

draw_graph(deg3_points, "deg3")
draw_graph(deg15_points, "deg15")