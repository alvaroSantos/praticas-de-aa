import numpy as np
import matplotlib.pyplot as plt

big_G = 6.67e-11
EY_to_s = 3.16e7
AU_to_m = 1.496e11

def sun_mass(velocity, radius):
    '''
    velocity -> in m/s
    radius -> m
    '''
    return (velocity**2 * radius)/big_G

def velocity(radius, period):
    '''
    radius -> in m
    period -> s
    '''
    return 2*np.pi*radius/period

def load_planet_data(file_name):
    """Return matrix with orbital radius and period, in this order"""
    rows = []
    lines = open(file_name).readlines()
    for line in lines[1:]:
        parts = line.split(',')
        rows.append((float(parts[1]),float(parts[2])))
    return np.array(rows)

def calc_masses(data):
    result = []
    data[:,0] = data[:,0]*AU_to_m
    data[:,1] = data[:,1]*EY_to_s
    for line in data:
        radius = line[0]
        period = line[1]
        v = velocity(radius, period)
        mass = sun_mass(v, radius)
        result.append(mass)
    return result

#Program
data = load_planet_data('planets.csv')
res = calc_masses(data)
mean = np.mean(res);
std_dv = np.std(res);

#Just for testing
print(mean)
print(std_dv)

#First part of the exercise is done
#The second part begins here
data = load_planet_data('planets.csv')
print(data)

x,y = (data[:,0], data[:,1])
coefs = np.polyfit(x,y,2)   #Polynomial of degree 2 fitting (lecture 2) using mean squared error

pxs = np.linspace(0,max(x),100) #Divides a line in 100 equal parts from 0 to max(x) (very similar to a range object in normal python)
poly = np.polyval(coefs,pxs)    #Faz coefs[0]*pxs[i]^2 + coefs[1]*pxs[i]^1 + coefs[2]*pxs[i], para cada ponto em pxs

plt.figure()
plt.plot(x,y,'or')  #pintar cada ponto (x,y)
plt.plot(pxs,poly,'-')  #pintar cada ponto (limite duma partição, valor do polinómio)
plt.axis([0,35, -50, 200])  #Para ficar com os ranges do exemplo
plt.title('Degree: 2')
plt.savefig('exercise1.png')
plt.close()