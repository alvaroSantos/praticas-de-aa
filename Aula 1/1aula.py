import numpy

def load_planet_data(file_name):
    """Return matrix with orbital radius and period"""
    rows = []
    lines = open(file_name).readlines()
    for line in lines[1:]:
        parts = line.split(',')
        rows.append((float(parts[1]),float(parts[2])))
    return numpy.array(rows)
    
data = load_planet_data('planets.csv')

print(data)

import matplotlib.pyplot as plt
plt.figure()
plt.plot(data[:,0], data[:, 1])
plt.savefig('planets.png', dpi=300)
plt.show()
plt.close()